document.querySelector("#like").addEventListener("click", async () => {
	document.querySelector("#like").disabled = true;
	const params = (new URL(location)).searchParams;
	const fet = await fetch(`/api/like?id=${params.get('q')}`, {
		method: "POST"
	});
});

document.querySelector("#dislike").addEventListener("click", async () => {
	document.querySelector("#dislike").disabled = true;
	const params = (new URL(location)).searchParams;
	const fet = await fetch(`/api/dislike?id=${params.get('q')}`, {
		method: "POST"
	});
});

document.querySelector("#comment").addEventListener("click", async () => {
	const params = (new URL(location)).searchParams;
	const text = document.querySelector("#com").value;
	const fet = await fetch(`/api/comment?id=${params.get('q')}`, {
		method: "POST",
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify({
			text: text
		})
	});
	const p = document.createElement("p");
	p.innerHTML = text;
	const hr = document.createElement("hr");
	document.querySelector("#comsec").appendChild(p);
	document.querySelector("#comsec").appendChild(hr);
});