document.querySelector("form").addEventListener("submit", (e) => {
	e.preventDefault();
	const vd = document.querySelector("input[type=file]").files[0];
	const title = document.querySelector("input[type=text]").value;
	const formdata = new FormData();
	formdata.append("video", vd);
	formdata.append("title", title);
	axios
		.post("/api/upload", formdata, {
			headers: {
				"Content-Type": "multipart/form-data"
			},
		})
		.then((res) => {
			window.location.href = '/'
		})
		.catch((err) => {
			console.log(err);
		});
});

// to do 
// use formidable/express-file-uploader instead of mutler
// kms