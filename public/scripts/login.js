document.querySelector("#f").addEventListener("submit", (e) => {
	e.preventDefault();
	if (e.submitter.defaultValue == "Register") {
		axios
			.post("/api/register", {
				username: document.querySelector("#username").value,
				password: document.querySelector("#password").value,
			})
			.then((res) => {
				console.log(res);
				if (res.data.message == "already exists") {
					console.log("already exists");
				}else{
					window.location.href = '/'
				}
			})
			.catch((err) => {console.log(err);});
	} else {
		axios
			.post("/api/login", {
				username: document.querySelector("#username").value,
				password: document.querySelector("#password").value,
			})
			.then((res) => {
				console.log(res);
				if (res.data.message == "does not exist") {
					console.log("bad credentials");
				}else{
					window.location.href = '/'
				}
			})
			.catch((err) => {console.log(err.message);});
	}
});
