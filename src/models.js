"use strict";
const { DataTypes } = require("sequelize");
const { sequelize } = require("./dbconnect");

const User = sequelize.define(
	"User",
	{
		id: {
			type: DataTypes.INTEGER,
			autoIncrement: true,
			primaryKey: true,
		},
		username: {
			type: DataTypes.STRING,
			allowNull: false,
			unique: true,
		},
		password: {
			type: DataTypes.STRING,
			allowNull: false,
		},
	},
	{
		timestamps: true,
		createdAt: "cta",
	}
);

const Upload = sequelize.define(
	"Upload",
	{
		id: {
			type: DataTypes.INTEGER,
			autoIncrement: true,
			primaryKey: true,
		},
		title: {
			type: DataTypes.STRING,
			allowNull: false,
		},
		name: {
			type: DataTypes.STRING,
			allowNull: false,
		},
		like: {
			type: DataTypes.INTEGER,
			allowNull: false,
			defaultValue: 0,
		},
		dislike: {
			type: DataTypes.INTEGER,
			allowNull: false,
			defaultValue: 0,
		},
	},
	{
		timestamps: true,
		createdAt: "cta",
	}
);

const Comment = sequelize.define("Comment", {
	id: {
		type: DataTypes.INTEGER,
		autoIncrement: true,
		primaryKey: true,
	},
	text: {
		type: DataTypes.TEXT,
		allowNull: false,
	},
});

User.hasMany(Upload, {
	foriegnKey: {
		name: "duh",
		type: DataTypes.INTEGER,
		allowNull: true,
	},
});
Upload.belongsTo(User);
Upload.hasMany(Comment, {
	foriegnKey: {
		name: "dudh",
		type: DataTypes.INTEGER,
		allowNull: true,
	},
});
Comment.belongsTo(Upload);

module.exports = {
	User,
	Upload,
	Comment,
};
