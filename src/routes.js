"use strict";
const control = require("./controllers");
const router = require("express").Router();
const checkAuth = require("./middleware/auth/checkAuth");

router.get("/", control.PageIndex);
router.get("/login", checkAuth, control.PageLogin);
router.get("/upload", checkAuth, control.PageUpload);
router.get("/watch", control.PageWatch);
router.get("/api/view", control.ApiView);
router.get("/api/logout", control.ApiLogout);
router.get("/api/red", control.ApiRedirect);
router.post("/api/register", control.ApiRegister);
router.post("/api/login", control.ApiLogin);
router.post("/api/upload", control.ApiUpload);
router.post("/api/like", control.ApiLike);
router.post("/api/dislike", control.ApiDislike);
router.post("/api/comment", control.ApiComment);
router.post("/api/logout", control.ApiLogout);


module.exports = router;