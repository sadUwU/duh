module.exports = (req, res, next) => {
	if(!req.cookies.token){
		req.auth = false;
		return next();
	}
	
	req.auth = true;
	next()
}