const jwt = require("jsonwebtoken");
const config = require("../../config");

function jwtSign(username, cta) {
	const token = jwt.sign(
		{
			username: username,
			cta: cta,
		},
		config.JWT_SECRET,
		{
			expiresIn: "2 days",
		}
	);
	return token;
}

function jwtVerify(token) {
	return jwt.verify(token, config.JWT_SECRET);
}

module.exports = {
	jwtSign,
	jwtVerify,
};
