"use strict";
const fs = require("fs");
const bcr = require("bcrypt");
const { User, Upload, Comment } = require("./models");
const formidable = require("formidable");
const jwt = require("jsonwebtoken");
const jwtUtils = require("./utils/jwt");

const ApiLogout = async (req, res) => {
	res.clearCookie("token");
	res.redirect("/");
};

const ApiRedirect = async (req, res) => {
	const { to } = req.query;
	res.redirect(to);
};

const ApiRegister = async (req, res) => {
	const { username, password } = req.body;
	if (!username || !password) {
		return res.status(400).json({ message: "bad credentials" });
	}
	const hash = await bcr.hash(password, 10);
	const newUser = User.build({
		username: username,
		password: hash,
	});
	newUser.save().catch((err) => {
		return res.status(500).json({ msg: "there was an error" });
	});
	const token = jwtUtils.jwtSign(
		newUser.dataValues.username,
		newUser.dataValues.cta
	);
	res.status(200).cookie("token", token).json({ msg: "successfull" });
	// error handling in save
};

const ApiLogin = async (req, res) => {
	const { username, password } = req.body;
	if (!username || !password) {
		return res.status(400).json({ message: "bad credentials" });
	}
	User.findOne({
		where: {
			username: username,
		},
	}).then(async (user) => {
		if (!user) {
			return res.status(400).json({ msg: "bad credentials" });
		}

		const truth = await bcr.compare(password, user.password);
		if (!truth) {
			return res.status(400).json({ msg: "bad credentials" });
		}

		const token = jwtUtils.jwtSign(
			user.dataValues.username,
			user.dataValues.cta
		);

		res.status(200).cookie("token", token).json({ msg: "successfull" });
	});
};

const ApiUpload = async (req, res, next) => {
	const form = formidable({
		multiples: true,
		maxFiles: 1,
		maxFileSize: 40 * 1024 * 1024,
		uploadDir: "./uploads",
		keepExtensions: true,
	});
	const jwtDecoded = await jwtUtils.jwtVerify(req.cookies.token);
	let userId;
	User.findOne({
		where: {
			username: jwtDecoded.username,
		},
	}).then((user) => {
		userId = user.dataValues.id;
		form.parse(req, (err, fields, files) => {
			if (err) {
				next(err);
				return;
			}
			if (!fields.title) {
				return res.status(400).json({ msg: "title not provided" });
			}

			const upload = Upload.build({
				title: `${fields.title}`,
				name: `${files["video"].newFilename}`,
				UserId: userId,
			});
			upload.save();
			return res.status(200).json({ msg: "upload succesfull" });
		});
	});
};

const ApiView = async (req, res) => {
	const { id } = req.query;
	if (!id || id < 0) {
		return res.status(400).json({ msg: "no id or invalid id" });
	}
	Upload.findOne({ where: { id: id } }).then((video) => {
		if (!video) {
			return res.status(404).json({ message: "video not found" });
		}

		if (!fs.existsSync(`./uploads/${video.dataValues.name}`)) {
			return res.status(500).json({ msg: "there was an error" });
		}

		res.status(200).sendFile(`uploads/${video.dataValues.name}`, {
			root: "./",
		});
	});
};

const ApiLike = async (req, res) => {
	const { id } = req.query;
	Upload.increment(
		{ like: 1 },
		{
			where: {
				id: id,
			},
		}
	);
	res.end();
};

const ApiDislike = async (req, res) => {
	const { id } = req.query;
	Upload.increment(
		{ dislike: 1 },
		{
			where: {
				id: id,
			},
		}
	);
	res.end();
};

const ApiComment = async (req, res) => {
	const { id } = req.query;
	const com = Comment.build({
		text: req.body.text,
		UploadId: id,
	}).save();
	res.end();
};

const PageIndex = async (req, res) => {
	const videos = await Upload.findAll({
		limit: 10,
	});
	const list = [];
	for (let i = 0; i < videos.length; i++) {
		list.push({
			title: videos[i].dataValues.title,
			id: videos[i].dataValues.id,
		});
	}
	res.render("index", { list: list });
};

const PageLogin = async (req, res) => {
	if (req.auth) {
		return res.status(301).redirect("/");
	}
	res.render("login");
};

const PageUpload = async (req, res) => {
	if (!req.auth) {
		return res.status(301).redirect("/login");
	}
	res.render("upload");
};

const PageWatch = async (req, res) => {
	const { q } = req.query;
	let comments = [];
	if (!q || q < 0 || isNaN(q)) {
		return res.status(400).render("404");
	}
	Upload.findOne({
		where: {
			id: q,
		},
		include: Comment,
	}).then((video) => {
		if (!video) {
			return res.status(404).render("404");
		}
		for (let i = 0; i < video.Comments.length; i++) {
			comments.push(video.Comments[i].dataValues.text);
		}
		res.render("watch", {
			id: q,
			title: video.dataValues.title,
			like: video.dataValues.like,
			dislike: video.dataValues.dislike,
			comments: comments,
		});
	});
};

module.exports = {
	ApiView,
	ApiLogout,
	ApiRedirect,
	ApiRegister,
	ApiLogin,
	ApiUpload,
	ApiLike,
	ApiDislike,
	ApiComment,
	PageIndex,
	PageLogin,
	PageUpload,
	PageWatch,
};
