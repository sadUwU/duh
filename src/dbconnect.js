"use strict";
const Seq = require("sequelize");
const config = require("../config");

const sequelize = new Seq(`postgres://${config.DATABASE_CONNECTION_STRING}`, {
	logging: config.LOG_LEVEL,
});

const connect = () => {
	try {
		sequelize.authenticate();
		sequelize.sync({ force: config.FORCE });
	} catch {
		process.exit(1);
	}
};

module.exports = {
	connect: connect,
	sequelize: sequelize,
};
