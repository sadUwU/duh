"use strict";
const express = require("express");
const config = require("../config");
const cookieParser = require("cookie-parser");
const session = require("express-session");

const app = express();

app.set("view engine", "ejs");

app.use(require("morgan")("dev"));
app.use(
	require("helmet").contentSecurityPolicy({
		directives: {
			defaultSrc: ["'self'", "data:", "blob:"],

			fontSrc: ["'self'", "https:", "data:"],

			scriptSrc: ["'self'", "unsafe-inline"],

			scriptSrc: ["'self'", "https://*.cloudflare.com"],
		},
	})
);
app.use(require("express-favicon")(__dirname + "/../public/favicon.ico"));
app.use(cookieParser())
app.use(express.json());
app.use(express.urlencoded({ extended: true }))
app.use('/static' ,express.static("./public"));

// routes
const routes = require("./routes");
app.use("/", routes);

app.use((req, res, next) => {
	res.render("404");
});

const { connect } = require("./dbconnect");

function start() {
	try {
		connect();
		app.listen(config.PORT);
		console.log("no problem");
	} catch (error) {
		console.log("problem");
		process.exit(1);
	}
}

start();
