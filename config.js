module.exports = {
	// upload folder
	UPLOAD_FOLDER: 'uploads/',
	// secret for sessions
	JWT_SECRET: "21F5735E8F2856F8CA815928E6CC58484E8E9B45D3683F9A51C851C5B6802F59",
	// secret for cookies
	JWT_SECRET: "856F8CA821F5735E8F2159C5B68E6CC58484E8E9B45802F592D3683F9A51C851",
	// db connection string 
	DATABASE_CONNECTION_STRING: 'postgres:451@localhost:5432/prj?schema=public',
	// port
	PORT: 80,
	// log level can be either false or console.log
	LOG_LEVEL: false,
	// force can be either false or true on production this should be flase
	FORCE: false
	
};
